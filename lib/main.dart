import 'dart:async';
import 'package:flutter/material.dart';
import 'package:animations/animations.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cougar Mail',
      theme: ThemeData(
        pageTransitionsTheme: PageTransitionsTheme(
          builders: {
            TargetPlatform.android: FadeThroughPageTransitionsBuilder(),
            TargetPlatform.iOS: FadeThroughPageTransitionsBuilder(),
          },
        ),
        primarySwatch: Colors.blue,
      ),
      home: FirstPage(),
      routes: {
        '/second': (_) => SecondPage(
              data: '',
            ),
        '/third': (_) => ThirdPage(
              data: '',
            ),
      },
    );
  }
}

class FirstPage extends StatefulWidget {
  @override
  FirstState createState() => FirstState();
}

class FirstState extends State<FirstPage> {
  String recipient;
  String subject;
  String body;
  String sent_recipient = 'No recipient';
  String sent_subject = 'No Subject';
  String sent_body = 'No Body';
  int index = 0;
  var sentItems = new List(30);
  bool isLoading = false;
  bool itemSent = false;

  void _sending() {
    setState(() {
      isLoading = true;
    });
    Timer.periodic(const Duration(seconds: 3), (t) {
      setState(() {
        _sent();
      });
      t.cancel();
    });
  }

  void _sent() {
    setState(() {
      sent_recipient = recipient;
      sent_subject = subject;
      sent_body = body;

      sentItems[index] = recipient;
      index = index + 1;
      sentItems[index] = subject;
      index = index + 1;
      sentItems[index] = body;
      index = index + 1;
      isLoading = false;
      Timer.periodic(const Duration(seconds: 3), (t) {
        setState(() {
          itemSent = false;
        });
        t.cancel();
      });
      itemSent = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cougar Email'),
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Compose Email:',
                style: TextStyle(fontSize: 20),
              ),
              TextFormField(
                decoration: InputDecoration(
                    fillColor: Colors.grey[300],
                    filled: true,
                    border: OutlineInputBorder(),
                    labelText: 'Email Recipient',
                    hintText: 'example@gmail.com'),
                onChanged: (value) => recipient = value,
              ),
              TextFormField(
                decoration: InputDecoration(
                    fillColor: Colors.grey[300],
                    filled: true,
                    border: OutlineInputBorder(),
                    labelText: 'Subject',
                    hintText: 'Subject Example'),
                onChanged: (value) => subject = value,
              ),
              TextFormField(
                minLines: 4,
                maxLines: 4,
                maxLengthEnforced: true,
                decoration: InputDecoration(
                    fillColor: Colors.grey[300],
                    filled: true,
                    border: OutlineInputBorder(),
                    labelText: 'Body',
                    hintText: 'Type Email Here'),
                onChanged: (value) => body = value,
              ),
              RaisedButton(
                color: Colors.blue,
                child: Text('Send Email'),
                onPressed: _sending,
              ),
              if (isLoading == true) CircularProgressIndicator(),
              if (isLoading == true)
                Text(
                  'SENDING...',
                  style: TextStyle(fontSize: 17),
                ),
              if (itemSent == true)
                Text(
                  'Email Sent!',
                  style: TextStyle(fontSize: 17),
                ),
              Row(
                children: [
                  Expanded(
                    child: RaisedButton(
                      color: Colors.blue,
                      child: Text('View Inbox'),
                      onPressed: () {
                        Navigator.of(context).pushNamed('/second');
                      },
                    ),
                  ),
                  Expanded(
                    child: RaisedButton(
                      color: Colors.blue,
                      child: Text('View Sent Emails'),
                      onPressed: () {
                        Navigator.of(context).pushNamed('/third');
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SecondPage extends StatelessWidget {
  final String data;

  SecondPage({
    Key key,
    @required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inbox'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _tile('Movie Date?', 'Sally123@example.com', Icons.theaters,
                '5:50 PM'),
            Divider(),
            _tile('Check Your Employee Portal!', 'csusmpayroll@example.com',
                Icons.money_off, '3:00 PM'),
            Divider(),
            _tile('CS 481 - Question on Lab 6?', 'spiderman@example.com',
                Icons.question_answer, '1:30 PM'),
            Divider(),
            _tile('Click now! Earn free rewards!', 'scammer@example.com',
                Icons.mouse, '7:58 AM'),
            Divider(),
            _tile('80% off! Black Friday Deals!', 'target@example.com',
                Icons.save, '6:00 AM'),
            Divider(),
            _tile('Youtube - Notifications(6)', 'youtube@example.com',
                Icons.notification_important, '5:14 AM'),
            Divider(),
            RaisedButton(
              child: Text('View Sent Emails'),
              onPressed: () {
                Navigator.of(context).pushNamed('/third');
              },
            ),
          ],
        ),
      ),
    );
  }

  ListTile _tile(
          String title, String subtitle, IconData icon, String trailing) =>
      ListTile(
        title: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 20,
          ),
        ),
        subtitle: Text(subtitle),
        leading: Icon(icon),
        trailing: Text(trailing),
      );
}

class ThirdPage extends StatelessWidget {
  final String data;

  ThirdPage({
    Key key,
    @required this.data,
  }) : super(key: key);

  ListTile _tile(String title, String subtitle, IconData icon, String trailing) => ListTile(
    title: Text(
      title,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 20,
      ),
    ),
    subtitle: Text(subtitle),
    leading: Icon(icon),
    trailing: Text(trailing),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sent Emails'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _tile("Did you get your portion of the lab done?", "aaron@cougars.csusm.edu", Icons.home,"4:45pm"),
            _tile("I'm looking for some computer parts", "ryan@cougars.csusm.edu", Icons.settings_input_composite,"11:45am"),
            _tile("Want to go play a game of soccer?", "alex@cougars.csusm.edu", Icons.score,"5:00pm"),
          ],
        ),
      ),
    );
  }
}
